# Luca de Alfaro, 2020
# BSD License

import json
from nqgcs import NQGCS
from tester import do, dont, do_all_tests


# We use a real datastore with test keys.
import os
KEY_FILE = "keys/test_keys.json"
BUCKET_NAME = 'camio-dev-test-bucket'
TEST_FILE = 'test_file'
TEST_STRING = 'ciao'

@do
def test_json_keys():
    c = NQGCS(json_key_path="keys/test_keys.json")
    c.write(BUCKET_NAME, TEST_FILE, TEST_STRING)
    s = c.read(BUCKET_NAME, TEST_FILE)
    assert s.decode('utf8') == TEST_STRING
    c.delete(BUCKET_NAME, TEST_FILE)

@do
def test_read_keys():
    with open(KEY_FILE, "r") as f:
        keys = json.load(f)
        c = NQGCS(keys=keys)
        c.write(BUCKET_NAME, TEST_FILE, TEST_STRING)
        s = c.read(BUCKET_NAME, TEST_FILE)
        assert s.decode('utf8') == TEST_STRING
        c.delete(BUCKET_NAME, TEST_FILE)

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = KEY_FILE
@do
def test_write_read_delete():
    c = NQGCS()
    c.write(BUCKET_NAME, TEST_FILE, TEST_STRING)
    s = c.read(BUCKET_NAME, TEST_FILE)
    assert s.decode('utf8') == TEST_STRING
    c.delete(BUCKET_NAME, TEST_FILE)

do_all_tests()
